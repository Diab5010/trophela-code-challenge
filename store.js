#!/usr/bin/env node
/* shebang line supported only on Unix-like platforms to use ./store.js instead of node store.js
*  for windows you can install the npm package globally and run the commands using the package name 'store'
*  or you can use GIT bash as it works like linux bash ;)
*/

var crudHelper = require("./crudHelper");
var command = process.argv[2];
var key = process.argv[3];
var value = process.argv[4];

switch (process.argv[2]) {
    case 'add':
        process.argv[3] !== undefined && process.argv[4] !== undefined ? crudHelper.add(process.argv[3], process.argv[4]) : console.log(`Missing Arguments, Please Specify the key & value after the "add" command !`);
        break;
    case 'list':
        crudHelper.list();
        break;
    case 'get':
        process.argv[3] !== undefined ? crudHelper.get(process.argv[3]):console.log(`Invalid Arguments, Missing The Key !`) ;
        break;
    case 'remove':
        process.argv[3] !== undefined ? crudHelper.remove(process.argv[3]):console.log(`Invalid Arguments, Missing The Key !`);
        break;
    case 'clear':
        crudHelper.clear();
        break;
    default:
    showHelp();
}

function showHelp(){
    console.log('OPTIONS:');
    console.log('- node store.js add mykey myvalue');
    console.log('- node store.js list');
    console.log('- node store.js get mykey');
    console.log('- node store.js remove mykey');
    console.log('- node store.js clear');
}