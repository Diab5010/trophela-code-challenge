const fileHelper = require('./fileHelper');

const add = function (key,value) {
    var element = {'key':key,'value':value};
    let data = fileHelper.readData();
    var obj = data.find(ele => ele.key === element.key) 
    if (obj) {
        console.log(`Sorry! Cann't insert data with replicated Key`);
    } else {
        data.push(element);
        if(fileHelper.writeData(data)){
            console.log(`Added Successfully :)`)
        }else{
            console.log(`Something Went Wrong Please Try Again Later !`)
        }
    }
}

const list = function () {
    let data = fileHelper.readData();
    if (data.length) {
        data.forEach((element) => {
            console.log(`${element.key}: ${element.value}`);
        });
    }else{
        console.log('No Data!')
    }
}

const get = function (key) {
    let data = fileHelper.readData();
    var element = data.find(ele => ele.key === key);
    return element ?  console.log(`${element.key}: ${element.value}`) :  console.log(`Not Found !`);;
}

const remove = function (key) {
    let data = fileHelper.readData();
    var newData = data.filter(ele => ele.key !== key);
    if(fileHelper.writeData(newData)){
        console.log(`Removed Successfully :)`)
    }else{
        console.log(`Something Went Wrong Please Try Again Later !`)
    }
}

const clear = function () {
    if(fileHelper.writeData([])){
        console.log(`Cleared Successfully :)`)
    }else{
        console.log(`Something Went Wrong Please Try Again Later !`)
    }
}

module.exports = {
    add,
    list,
    get,
    remove,
    clear,
};