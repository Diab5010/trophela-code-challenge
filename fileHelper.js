//import Required Files
const fs = require('fs');
//Decalre Variables
const filePath = './data.json';
//Declare functions
const isFileExists = () => fs.existsSync(filePath);
const createFileIfNotExisted = () => {
    if (!isFileExists()) {
        fs.appendFileSync(filePath, '{"data":[]}')
    }
};
const readData = () => {
    createFileIfNotExisted();
    const fileData = fs.readFileSync(filePath);
    return JSON.parse(fileData).data;
}
const writeData = (data) => {
    try {
        createFileIfNotExisted();
        fs.writeFileSync(filePath, JSON.stringify({"data":data}));
        return true;
    } catch (error) {
        return false;
    }
};

module.exports = {
    readData,
    writeData
}